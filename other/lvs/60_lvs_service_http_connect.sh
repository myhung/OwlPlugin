#!/bin/bash
#===============================================================================
#   DESCRIPTION:lvs 是否正常! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/08/09 16:31
#2017 1 9:增加 lvs 443 启用扫描;修改/var/log/messages 只读最后200条记录
#2017-1-23 增加rs ActiveConn 连接状态监控，当InActConn大于100时
#判断ActiveConn是否为0，为0发出告警
#2017-1-23 17:53 修改ActiveConn 连接状态监控逻辑,当ActiveConn所有连接大于20时，开始
#2017-2-6  修改ActiveConn 连接状态监控逻辑,当ActiveConn所有连接大于80时，开始
#判断rs的最小值，为0时，做告警
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

function LVS_Removing
{
    if [ -f /var/log/messages ];then	
       local value=$(tail -n200 /var/log/messages|egrep Keepalived |egrep "$minutes"|egrep -v "VRRP_Instance|invalid passwd"|grep -c Removing)
        echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs.removing\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}, 
	else
	  exit;
    fi
}

function LVS_443_status
{
local IPlist=$(ipvsadm -ln |grep TCP|awk '{print $2}'|egrep -w  443)
    for VIP in $IPlist
     do
      local value=$(ipvsadm -ln -t $VIP|egrep -c Route)
        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"lvsvip=$VIP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs.443.port\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},
   done 
	
}
ipvsadm_ActiveConn(){

local VIPlist=$(ipvsadm -ln |grep TCP|awk '{print $2}'|egrep -wE  '80|443')

for vip in $VIPlist
  do
  ActiveConn_sum=$(ipvsadm -lnt $vip|grep -E 'TCP|Route'|grep Route|awk '{sum+=$5}END{print sum}')
  if [ $ActiveConn_sum -gt 3000 ];then
     ActiveConn=$(ipvsadm -lnt $vip|grep -E 'TCP|Route'|grep Route|awk '{print $5}'| \
                  awk 'BEGIN {min = 1999999} {if ($1+0 < min+0) min=$1} END {print min}')
   echo -n {\"endpoint\": \"$endpoint\",\
           \"tags\": \"vip=$vip\",\
           \"timestamp\": $timestamp,\
           \"metric\": \"service.http.lvs.ActiveConn\",\
           \"value\": $ActiveConn, \
           \"counterType\": \"GAUGE\", \"step\": 60},
  fi
done

}	
function LVS_80_status
{
local IPlist=$(ipvsadm -ln |grep TCP|awk '{print $2}'|egrep -w  80)
    for VIP in $IPlist
     do
      local value=$(ipvsadm -ln -t $VIP|egrep -c Route)
      echo -n {\"endpoint\": \"$endpoint\",\
        \"tags\": \"lvsvip=$VIP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs.status\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},
    done|sed -e 's/,$/]/'
					
}
minutes=$(date +"%b %e %H:%M:")
endpoint=$(hostname -s)
timestamp=$(date +%s)

#监控切换到/home/owl/插件监控
if [ -f /etc/init.d/keepalived ];then
#	LVS_Removing
#	LVS_443_status
#	ipvsadm_ActiveConn
#	LVS_80_status
#    else
	  exit;
fi
