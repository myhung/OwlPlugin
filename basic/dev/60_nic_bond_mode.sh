#!/bin/bash
# jiangbin
# 2016-6-22
# print interface bond mode

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
timestamp=`date +%s`
default_netcard=`awk '$2 == 00000000 { print $1 }' /proc/net/route`

if [ -f "/sys/class/net/$default_netcard/bonding/mode" ]; then 
    value=`cat /sys/class/net/$default_netcard/bonding/mode|awk '{print $2}'`
    echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"nic.bond.mode\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
else
    echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"nic.bond.mode\", \"value\": -1, \"counterType\": \"GAUGE\", \"step\": 60}]
fi
