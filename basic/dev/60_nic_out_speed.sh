#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_nic_out_speed.sh
# Revision:     1.0
# Date:         2016/08/04
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  采集本机所有网卡的速率
# -------------------------------------------------------------------------------
# Revision 1.0
# 采集本机所有网卡的速率
# nic.default.out.speed + tag(默认出口网卡名称),打印默认出出口网卡的速率
# nic.out.speed + tag(网卡名称),打印所有出口网卡的速率

# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
timestamp=`date +%s`

echo -n [

awk '{print $1}' /proc/net/dev|grep ':'|grep -v lo|awk -F':' '{print $1}'| while read line

do 
    if [ `cat /sys/class/net/$line/operstate` == "up" ]; then
        value=`cat /sys/class/net/$line/speed`
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"device=$line\", \"timestamp\": $timestamp, \"metric\": \"nic.out.speed\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60},
    fi
done

default_netcard=`awk '$2 == 00000000 { print $1 }' /proc/net/route`
value=`cat /sys/class/net/$default_netcard/speed`

echo -n {\"endpoint\": \"$endpoint\",\"timestamp\": $timestamp, \"metric\": \"nic.default.out.speed\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]

