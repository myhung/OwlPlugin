#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Going Down
# 2: No speed

# Check Eth Link Speed.
#   Return error str if found error, otherwise return str "OK".

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function nic_stat() {

    local nic_record_file="/tmp/.NIC_LINK_SPEED_RECORD_BY_ZABBIX_AGENT.record"

    # check bond:
    grep ":" /proc/net/dev | awk '{sub(/:/," "); print $1}' | grep -q "bond"
    if [ $? -eq 0 ] ; then
        local skip_null_ip=0
    else
        # skip interface without ip address:
        local skip_null_ip=1
    fi

    for device in $(grep ":" /proc/net/dev | grep -v bond | awk '{sub(/:/," "); print $1}') ; do

        # Link status:
        ifconfig ${device} | grep -q -w "UP"
        # Link down, skip:
        [ $? -ne 0 ] && continue

        # Link without ip and this server doesn't have bond interafec:
        /sbin/ip addr show ${device} | grep -q "inet [0-9]*.[0-9]*.[0-9]*.[0-9]*"
        if [ $? -ne 0 ] && [ ${skip_null_ip} -eq 1 ] ; then
            continue
        fi

        local link_speed=$(sudo /sbin/ethtool ${device} | awk '/Speed/{print $2}')

        # skip bond/lo:
        if [ -z "${link_speed}" ] ; then
            continue
        fi

        # Link has speed (not "Unknown!" or something else):
        echo "${link_speed}" | grep -q "Mb/s"
        if [ $? -eq 0 ] ; then
            #$local net_speed=`sar -n DEV |tail -n 10 | grep -v Average |grep $device |tail -n1 |awk '{printf "%d",$7*8/1000;exit}'`
            #$local nic_speed=${link_speed%%Mb/s}
            #$( echo "$net_speed" | egrep -q '^[0-9]+$' ) || continue
            # 是否带宽跑满, 超过95% ?
            #$if [ `echo "scale=2; $net_speed/$nic_speed*100" |bc |sed 's/...$//'` -ge 95 ]; then
            #$    echo "$device: ${net_speed}Mbps"
            #$    return 0
            #$fi

            # Has record file:
            if [ -f "${nic_record_file}" ] ; then

                local last_link_speed=$(grep "${device}" "${nic_record_file}" | awk '{print $2}')

                # Doesn't have record:
                if [ -z "${last_link_speed}" ] ; then
                    echo "${device} ${link_speed}" >> "${nic_record_file}"
                else

                    # Grown up or Going down:
                    if [ "x${last_link_speed}" != "x${link_speed}" ] ; then

                        local last_speed=${last_link_speed%%Mb/s}
                        local curr_speed=${link_speed%%Mb/s}

                        # Grown up:
                        if [ ${last_speed} -lt ${curr_speed} ] ; then
                            sed -i "/${device} /d" "${nic_record_file}"
                            echo "${device} ${link_speed}" >> "${nic_record_file}"
                        # Going down:
                        else
                            # Error:
                            echo "${device} ${last_link_speed}->${link_speed}"
                            return 1
                        fi

                    fi

                fi

            else
                echo "${device} ${link_speed}" >> "${nic_record_file}"
            fi

            # No error found on this nic, contiue to next:
            continue

        else
            # error:
            echo "${device} ${link_speed}"
            return 2
        fi


    done

    echo "OK"

    return 0

}

# Call function
msg=$(nic_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Parse message
if [ 0 != "$retval" ] ; then
    tag=$(echo $msg | awk '{print "device=" $1}')
fi

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.nic.speed\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
