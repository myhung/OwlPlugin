#!/bin/bash

#===============================================================================
#   DESCRIPTION:计费日志待上传FTP过多 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/3/10 
#===============================================================================

data_to_ftp(){
    if [ -d /cache/logs/data_to_ftp/ ];then

	file_sum=$(ls /cache/logs/data_to_ftp/|egrep '\.[a-z]+'|wc -l)

    else 

	file_sum="-1"

    fi	

	
cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.cache.data.to.ftp",
        "value"         :$file_sum,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

endpoint=$(hostname -s)
timestamp=$(date +%s)


  data_to_ftp 


