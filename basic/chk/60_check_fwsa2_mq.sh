#!/bin/bash
#filename: check_fwsa2_mq.sh
#Athor: 季豪杰
#date:2016-9-23
#Revision:     1.0
#description : 监测 fwsa2连接 mq  结果超过3 个异常。

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function check(){

    answer=$(tail  /cache/logs/devops/fwsa2/fwsa2.log |egrep 'call_func":"(dial|notifyChannel|createQueues|channel)",.*,"level":"error"'|wc -l)

    if [[ $answer -gt 3 ]];then
        return 1
    else
        return 0
	fi
}

ret=$(check)
retval1=$?
date=`date +%s`
host=$HOSTNAME
tag=""


echo -n "[{
    \"endpoint\"   : \"$host\",
    \"tags\"       : \"$tag\",
    \"timestamp\"  : $date,
    \"metric\"     : \"check.fwsa2\",
    \"value\"      : $retval1,
    \"counterType\": \"GAUGE\",
    \"step\"       : 60}]"
