#!/bin/bash
#   DESCRIPTION:fwredis 时间戳监控 如果获取到的时间戳比当前时间戳早 3 分钟或以上就告警 
#               return 1 ERR
#               return 0 OK
#       设备组:fatcache 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/12 
#================================

fwredis_time(){
    date_time=$(date '+%s')
    fwredis_time=$(/FastwebApp/fwredis/bin/redis-cli -p 6497 get last_timestamp |egrep [0-9])
    TIME=$(expr \( $date_time - $fwredis_time \) / 60)
    if [ $TIME -gt 3 ];then
	return 1
     else
        return 0
   fi
}

##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
fwredis=$(ls /FastwebApp/fwredis/bin/redis-cli |wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $fwredis -gt 0 ]]; then
	mag=$(fwredis_time)
        value=$?	
else
    exit;
fi
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fwredis.diff.time\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}]
