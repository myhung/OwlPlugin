#!/bin/bash
########################################
#DESCRIPTION: fwsa 刷新失败监控
#grep '"when":"刷新 URL"'|grep '"level":"error"' 失败字段，
#cut -d'"' -f42 |cut -d '/' -f1 失败域名
#AUTHOR:  韦启胜
#CREATED: 2017/1/4
#########################################

main() {
    check_purge
    makejson
}
check_purge() {

        DOMAIN=$(tail /cache/logs/devops/fwsa2/fwsa2.log|grep '"when":"刷新 URL"'|grep '"level":"error"'| cut -d'"' -f42 |cut -d '/' -f1|head -n1)

        if [[ -n $DOMAIN ]]; then
            value=1
          else
           value=0 
        fi

}

makejson(){

if [[ -n $DOMAIN ]];then
cat << EOF
[
    {
        "endpoint"      :"$endpoint",
        "tags"          :"error=$DOMAIN",
        "timestamp"     :$timestamp,
        "metric"        :"service.cache.http.purge",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :60
    }
]
EOF
else
cat << EOF
[
    {
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.cache.http.purge",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :60
    }
]
EOF
fi
}
#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=$(hostname -s)
timestamp=$(date +%s)

main


