#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_dns_check.sh
# Revision:     v0.1
# Date:         2017/1/5
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Metric value definition:
# 0: OK
# 1: Error
# -------------------------------------------------------------------------------
# Revision v0.1
# Description:	探测DNS是否正常，不正常retry 3次
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function dns_check {
    local dns_host1=${1}
    local dns_host2=${2}

    dns_ip=`host -W 1 $dns_host1 |grep -oP '(\d{1,3}\.){3}\d{1,3}' | head -1`

    if [ -z $dns_ip ]; then
        # One more try for live streaming media.

        dns_ip2=`host -W 1 $dns_host2 |grep -oP '(\d{1,3}\.){3}\d{1,3}' | head -1`
        if [ -z $dns_ip2 ]; then
            # ERROR
            echo 1
            return 1
        else
            # OK
            echo 0
            exit 0
        fi

    else
        # OK
        echo 0
        exit 0
    fi
}

# Call function
msg=$(dns_check "www.taobao.com" "www.fastweb.com.cn")
MSG=$?
if [ $MSG -eq 1 ];then
    for retry in `seq 1 3`
    do
        dns_check "www.taobao.com" "www.fastweb.com.cn"
        if [ $? -eq 0 ];then
            MSG=0
            break
        else
            MSG=1
            sleep 1
            continue
        fi
    done
fi
DATE=`date +%s`
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$HOST",
    "tags"       : "$TAG",
    "timestamp"  : $DATE,
    "metric"     : "chk.dns_check",
    "value"      : $MSG,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
