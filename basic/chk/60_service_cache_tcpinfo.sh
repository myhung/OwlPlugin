#!/bin/bash

#===============================================================================
#   DESCRIPTION:tcp 优化安装监控 数据流监控! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/03/16 
# 修正数法 expr >> bc
#===============================================================================

process_appex (){

    appex=$(lsmod | grep -q appex && echo 0 || echo 1)
}

Num_Flows(){
    if [ $appex -eq 0 ];then
     Num_Flows=$(/etc/init.d/lotServer status 2>&1|grep NumOfTcpFlows|awk '{print $2}')
    else
     Num_Flows=0
    fi
}
tcp_flows (){
        
    if [ $appex -eq 0 -a $Num_Flows -gt 1000 ];then

        NumOfTcpFlows=$(/etc/init.d/lotServer status|grep NumOfTcpFlows|awk '{print $2}')
        TotalAccTcpFlow=$(/etc/init.d/lotServer status|grep TotalAccTcpFlow|awk '{print $2}')
        SUM=$(echo "scale=3;($NumOfTcpFlows - $TotalAccTcpFlow)/ $TotalAccTcpFlow * 100"|bc)
     else
        SUM=0
    fi
}

print_log (){
   if [ $SUM -gt 50 ];then
     /etc/init.d/lotServer status >> /tmp/tcp_monitor.log 2>&1
     echo "$(date +'%Y-%m-%d_%H:%M')" >> /tmp/tcp_monitor.log  2>&1
   fi
}

makejson(){

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.lotServer.chk.install",
        "value"         :$appex,
        "counterType"   :"GAUGE",
        "step"          :60
    },

        {
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.lotServer.chk.flow",
        "value"         :$SUM,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

endpoint=$(hostname -s)
timestamp=$(date +%s)

    process_appex
    Num_Flows
    tcp_flows
    print_log
    makejson
