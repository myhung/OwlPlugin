#!/bin/bash
#====================================================
#DESCRIPTION: SSD 寿命 监控
#smartctl -A /dev/sdn|egrep "Available_Reservd_Space"
#预留空间剩余量 Available Reserved Space（Intel芯片）
#重映射扇区计数 Reallocated Sectors Count/ 退役块计数 Retired Block Count [判断临界值，越低越好]
#Spin_Retry_Count 主轴起旋重试次数（即硬盘主轴电机启动重试次数） [判断临界值，越低越好]
#Reallocation Event Count 重定位事件计数 记录已重映射扇区和可能重映射扇区的事件计数 [越低越好]
#Current_Pending_Sector 记录不稳定扇区[越低越好]
#Offline_Uncorrectable 记录出错的扇区数量 [越低越好]

#AUTHOR:  韦启胜
#CREATED: 2017/1/1
#====================================================

#ssd 以当前值判断
smartctl_ssd() {

    local smartctl=(Media_Wearout_Indicator Available_Reservd_Space)

    echo -n "["
    for i in $(mount | awk '{print $1}' | grep '^/' |sed 's/[0-9]//g'| sort | uniq -c | awk '{print $2}');do
       for number in ${smartctl[@]};do
           VALUE_SSD=$(smartctl -A $i|grep "$number"|awk '{print $4}')
       if [[ -n $VALUE_SSD ]]; then

       echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"device=$i\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"sys.disk.smart.$number\",\
            \"value\": \"$VALUE_SSD\", \
            \"counterType\": \"GAUGE\", \"step\": 600},
     fi

    done
done
}
smartctl_info(){
     local smartctl=(Reallocated_Sector_Ct Spin_Retry_Count Reallocated_Event_Count Current_Pending_Sector Offline_Uncorrectable)
     for i in $(mount | awk '{print $1}' | grep '^/' |sed 's/[0-9]//g'| sort | uniq -c | awk '{print $2}');do
     for Thresh in ${smartctl[@]};do
       Threshold=$(smartctl -A $i|grep "$Thresh"|awk '{print $6}')

      if [[ -n $Threshold ]];then
              
      echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"device=$i\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"sys.disk.smart.$Thresh\",\
            \"value\": \"$Threshold\", \
            \"counterType\": \"GAUGE\", \"step\": 600},
      fi
done
done|sed -e 's/,$/]/'
}
#########################################################################
[ ! -x /usr/sbin/smartctl ] && yum install -y smartmontools
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=$(hostname -s)
timestamp=$(date +%s)

smartctl_ssd
smartctl_info
