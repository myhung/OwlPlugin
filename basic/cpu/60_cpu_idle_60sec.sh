#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_cpu_idle_60sec.sh 
# Revision:     1.0
# Date:         2017/3/20
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function t1_data(){
cat /proc/stat |grep -w cpu|awk '{for(i=2;i<=NF;i++)sum+=$i;print $5,sum}'
}

function t2_data(){
sleep 30
cat /proc/stat |grep -w cpu|awk '{for(i=2;i<=NF;i++)sum+=$i;print $5,sum}' |while read idle total
do
printf "%d%15d\n" $(expr $idle - $1) $(expr $total - $2)
done
}
function cpu_idle_60sec(){
t2_data $(t1_data) |awk '{printf "%d\n",$1/$2*100}'
}
#---------------------------------------------------------------------------------

# Call function
msg=$(cpu_idle_60sec)
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "cpu.idle.60sec",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF

