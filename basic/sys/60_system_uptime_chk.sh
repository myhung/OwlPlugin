#!/bin/bash

#===============================================================================
#   DESCRIPTION:系统重启监控
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/03/21 
#20170323:优化uptime判断 -v day
#20170411：没有数据时输出0
#===============================================================================

up_null(){
   local uptime=$(uptime | awk '$4=="min," {print $3}')
if [[ -z $uptime ]];then
    value=0
else
    value=$uptime
    up_time
fi
}

up_time(){
if [ $uptime -lt 5 ];then

    value=1
 else
    value=0

fi
}
makejson(){

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"system.uptime.chk",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

endpoint=$(hostname -s)
timestamp=$(date +%s)

   up_null
   makejson
