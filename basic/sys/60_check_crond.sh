#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_crond.sh 
# Revision:     1.0
# Date:         2017/1/11
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  监控crond进程
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function check_crond()
{
ps -ef|grep crond|awk '$8~/crond/{print}'|wc -l
}
#---------------------------------------------------------------------------------

# Call function
msg=$(check_crond)
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "check.crond",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
