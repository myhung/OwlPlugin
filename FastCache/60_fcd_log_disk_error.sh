#!/bin/bash


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fcd_log_disk_error() {
    local log_file=`ls -t /cache/logs/*error.log | head -1`
    grep "disk read failed" $log_file 2>/dev/null | wc -l
}

msg=$(fcd_log_disk_error)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"fcd.log.disk.error\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
