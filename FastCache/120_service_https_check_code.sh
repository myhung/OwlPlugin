#!/bin/bash
########################################
#DESCRIPTION: c01.i02 https 多玩域名https 监控
#curl 请求url 是否200 
#AUTHOR:  韦启胜
#CREATED: 2016/12/23 
#20170410 增加yydl域名探测
#########################################


#curl 设置3秒超时，超时间隔20秒重试3次
HTTPS_CODE() {

  https_mp4=$(curl --insecure -so /dev/null  -w '%{http_code}'  -H "Host: w5.dwstatic.com" "https://127.0.0.1/do_not_delete/t.mp4" -m 3 --retry-delay 20 --retry 3)
  https_yydl=$(curl --insecure -so /dev/null  -w '%{http_code}'  -H "Host: yydl.duowan.com" "https://127.0.0.1/mobile/yyst-android/2.0.0/yyst-2.0.0.apk" -m 3 --retry-delay 20 --retry 3)

}
code_value(){

   value=($https_mp4 $https_yydl)

}
makejson(){

   endpoint=$(hostname -s)
   timestamp=$(date +%s)

i=0
echo -n "["
CHAR=(',')

for domain in w5.dwstatic.com yydl.duowan.com
do
cat << EOF
  {"endpoint"   : "$endpoint",
  "tags"       : "fail=$domain",
  "timestamp"  : $timestamp,
  "metric"     : "service.https.check.code",
  "value"      : ${value[$i]},
  "counterType": "GAUGE",
  "step"       : 120}${CHAR[$i]}
EOF
((i++))
done

echo -n "]"
}



#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
C01_i02=$(/FastwebApp/fwutils/bin/fwhoami|grep -c c01.i02)

if [ $C01_i02 -gt 0 ]; then
    HTTPS_CODE
    code_value
    makejson
  else
    exit;
fi
