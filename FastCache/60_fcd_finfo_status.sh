#!/bin/bash
#===============================================================================
#   DESCRIPTION: 提取 FCD 的 finfo 命令的数值，包括：
#           -f 参数的 FD Total、FD Clients、FD Pipe、FD Upstream、FD Resolver、FD File
#           -H 参数的 Hit、Miss、Deny、Fetch、Events、Requesting、Cache Hit、Size Hit
#           -l 参数的 LRU Mem Objs
#        AUTHOR: 高一铠(gaoyk@fastweb.com.cn)
#       CREATED: 2017.02.15 11:06
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

if [ -f /usr/local/fastcache/bin/finfo ]; then
    FINFO_MSG=$(/usr/local/fastcache/bin/finfo -o -H -f $(cat /usr/local/fastcache/etc/fastcache.conf | grep '^server.stat_runtime'|grep -o '/.*/fcache_stat_runtime.log')|grep -v '\-\-\-\-\-')
    declare -A FINFO_DICT
    FINFO_DICT=(
    [fcd.fd.total]="$(echo ${FINFO_MSG} | awk -F '|' '{print $3}')_GAUGE"
    [fcd.fd.clients]="$(echo ${FINFO_MSG} | awk -F '|' '{print $7}')_GAUGE"
    [fcd.fd.pipe]="$(echo ${FINFO_MSG} | awk -F '|' '{print $12}')_GAUGE"
    [fcd.fd.upstream]="$(echo ${FINFO_MSG} | awk -F '|' '{print $14}')_GAUGE"
    [fcd.fd.resolver]="$(echo ${FINFO_MSG} | awk -F '|' '{print $16}')_GAUGE"
    [fcd.fd.file]="$(echo ${FINFO_MSG} | awk -F '|' '{print $18}')_GAUGE"
    [fcd.http.hit_count]="$(echo ${FINFO_MSG} | awk -F '|' '{print $24}')_COUNTER"
    [fcd.http.miss_count]="$(echo ${FINFO_MSG} | awk -F '|' '{print $26}')_COUNTER"
    [fcd.http.deny_count]="$(echo ${FINFO_MSG} | awk -F '|' '{print $28}')_COUNTER"
    [fcd.http.fetch_count]="$(echo ${FINFO_MSG} | awk -F '|' '{print $30}')_COUNTER"
    [fcd.http.events]="$(echo ${FINFO_MSG} | awk -F '|' '{print $33}')_GAUGE"
    [fcd.http.requesting]="$(echo ${FINFO_MSG} | awk -F '|' '{print $35}')_GAUGE"
    [fcd.http.cache_hit]="$(echo ${FINFO_MSG} | awk -F '|' '{gsub(" %","",$37);gsub("-nan","0",$37);print $37}')_GAUGE"
    [fcd.http.size_hit]="$(echo ${FINFO_MSG} | awk -F '|' '{gsub(" %","",$39);gsub("-nan","0",$39);print $39}')_GAUGE"
    [fcd.lru.mem_objs]="$(/usr/local/fastcache/bin/finfo -o -l /usr/local/fastcache/logs/fcache_stat_runtime.log|grep 'LRU Total Objs'|awk -F '|' '{sum+=$7}END{print sum}')_COUNTER"
    )
else
    exit 1
fi


HOST=$(hostname -s)
DATE=$(date "+%s")
NUM=1
echo '['
for METRIC in $(echo ${!FINFO_DICT[*]}); do
    VALUE=$(echo ${FINFO_DICT[${METRIC}]} | awk -F '_' '{print $1}')
    COUNTER_TYPE=$(echo ${FINFO_DICT[${METRIC}]} | awk -F '_' '{print $2}')
    if [ ${NUM} -ne ${#FINFO_DICT[*]} ]; then
cat << EOF
    {
        "endpoint"      :"${HOST}",
        "tags"          :"",
        "timestamp"     :${DATE},
        "metric"        :"${METRIC}",
        "value"         :${VALUE},
        "counterType"   :"${COUNTER_TYPE}",
        "step"          :60
    },
EOF
    else
cat << EOF
    {
        "endpoint"      :"${HOST}",
        "tags"          :"",
        "timestamp"     :${DATE},
        "metric"        :"${METRIC}",
        "value"         :${VALUE},
        "counterType"   :"${COUNTER_TYPE}",
        "step"          :60
    }
EOF
    fi
    ((NUM++))
done
echo ']'
