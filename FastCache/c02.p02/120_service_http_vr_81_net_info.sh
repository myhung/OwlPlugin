#!/bin/bash
#===============================================================================
#   DESCRIPTION:cache fcd 军网vr.81.cn回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/19
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function god_ip_alive
{
        local value=$(httping $ip -N 1 -G -c10 -t1 -f > /dev/null;echo $?) 

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"upstreams_alive=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fastcache.net.alive.up\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, 
         
}
function httpingloss
{
       local value=$(httping $ip -G -c10 -t1 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"upstreams_loss=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fastcache.net.upstreams.s\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},  
}
function httpingms
{
        local value=$(httping $ip -G -c10 -t1 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')
        if [[ -n $value ]];then

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"upstreams_ms=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fastcache.net.upstreams.s\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},
      fi
}
speed_download()

{
    SPEED=$(/usr/bin/curl -s -o /dev/null -r 0-10485760 -w '%{speed_download}' \
    'http://vr.81.cn/content/utovr/video/lieying_mgz_hx6/lieying_mgz_hx6.mp4' -x $ip:80  -m 3)
    value=$(echo "$SPEED" '*' "8" | bc)
    echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"军网源speed=$ip\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"service.net.speed.vr\",\
            \"value\": $value, \
            \"counterType\": \"GAUGE\", \"step\": 120},
}

#####################################################
FCD_c02_p02=$(/FastwebApp/fwutils/bin/fwhoami |grep -c c02.p02)
if [ ${FCD_c02_p02} -gt 0 ];then
[ ! -x  /usr/bin/httping ] &&  yum install -y httping-2.4
INNERDNS=$(grep 'server.dns' /usr/local/fastcache/etc/fastcache.conf|grep -v '#'|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
for INNER in ${INNERDNS[@]};do
        PERIP=$(host -W 1 vr.81.cn $INNER|grep address|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
done
IPlist=$(echo "$PERIP" |sort|uniq -c|awk '{print $2}')
endpoint=$(hostname -s)
timestamp=$(date +%s)
#########################
    thread=20
    tmp_fifofile=/tmp/$$.fifo
    mkfifo $tmp_fifofile
    exec 6<>$tmp_fifofile
    rm $tmp_fifofile 

for ((i=0;i<$thread;i++));do
        echo 
done >&6

echo -n "["
 for ip in ${IPlist[@]};do 
    read -u6
    {
      god_ip_alive
      httpingloss
      httpingms
      speed_download
      echo >&6
    } &
  done|sed -e 's/,$/]/'
wait

exec 6>&-
exit 0
else
exit;
fi
