#!/bin/bash
#===============================================================================
#   DESCRIPTION:cache fcd kw.m.l.cztv.com 回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/4/22
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function god_ip_alive
{
local value=$(curl -I -so /dev/null -w '%{http_code}' http://kw.m.l.cztv.com/channels/lantian/channel01/360p.m3u8 -x $ip:80 -m 3) 

      echo -n {\"endpoint\": \"$endpoint\",\
              \"tags\": \"upstreams_alive=$ip\",\
              \"timestamp\": $timestamp,\
              \"metric\": \"service.fastcache.net.alive.up\",\
              \"value\": $value, \
              \"counterType\": \"GAUGE\", \"step\": 120}, 
         
}
function httpingloss
{
local value=$(httping $ip -G -c10 -t1 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

      echo -n {\"endpoint\": \"$endpoint\",\
              \"tags\": \"upstreams_loss=$ip\",\
              \"timestamp\": $timestamp,\
              \"metric\": \"service.fastcache.net.upstreams.o\",\
              \"value\": $value, \
              \"counterType\": \"GAUGE\", \"step\": 120},  
}
function httpingms
{
local value=$(httping $ip -G -c10 -t1 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')
if [[ -n $value ]];then

   echo -n {\"endpoint\": \"$endpoint\",\
           \"tags\": \"upstreams_ms=$ip\",\
           \"timestamp\": $timestamp,\
           \"metric\": \"service.fastcache.net.upstreams.s\",\
           \"value\": $value, \
           \"counterType\": \"GAUGE\", \"step\": 120},
fi
}
speed_download()
{

SPEED=$(/usr/bin/curl -s -o /dev/null -w '%{speed_download}' \
      'http://kw.m.l.cztv.com/channels/lantian/channel01/360p.m3u8' -x $ip:80  -m 3)
value=$(echo "$SPEED" '*' "8" | bc)
    echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"upstream_ip=$ip\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"service.net.speed.hls\",\
            \"value\": $value, \
            \"counterType\": \"GAUGE\", \"step\": 120},
}

#####################################################
proc_sum=$(ps aux|grep -c $0)
FCD_HLS=$(/FastwebApp/fwutils/bin/fwhoami|egrep -c 'hls.super|hls.parent|c05.i08')
TMP_FILE="/tmp/.$0.txt"

if [ ${FCD_HLS} -gt 0 -a $proc_sum -eq 3 ];then

[ ! -x  /usr/bin/httping ] &&  yum install -y httping-2.4

INNERDNS=$(grep 'server.dns' /usr/local/fastcache/etc/fastcache.conf|grep -v '#'|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
for INNER in ${INNERDNS[@]};do
        PERIP=$(host -W 1 kw.m.l.cztv.com $INNER|grep address|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' >> $TMP_FILE 2>&1)
done

IPlist=$(awk '!a[$0]++ {print $0}' $TMP_FILE)
echo > $TMP_FILE 2>&1

endpoint=$(hostname -s)
timestamp=$(date +%s)
#########################
    thread=20
    tmp_fifofile=/tmp/$$.fifo
    mkfifo $tmp_fifofile
    exec 6<>$tmp_fifofile
    rm $tmp_fifofile 

for ((i=0;i<$thread;i++));do
        echo 
done >&6

echo -n "["
 for ip in ${IPlist[@]};do 
    read -u6
    {
      god_ip_alive
      httpingloss
      httpingms
      speed_download
      echo >&6
    } &
  done|sed -e 's/,$/]/'
wait

exec 6>&-
exit 0
else
exit;
fi
