#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_heka_cronolog.sh
# Revision:     1.0
# Date:         2016/12/30
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  监控heka_cronolog日志进程
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function heka_cronolog()
{
cron_count=`ps -ef|grep "/cache/logs/heka-cronolog/%Y%m%d%H%M/fcd*"|grep -v grep |wc -l`

if [ $cron_count -lt 1 ];then
    return 1
else
    ls -R  /cache/logs/heka-cronolog |grep -q '^fcd-' && return 0 || return 1
fi 
}
#---------------------------------------------------------------------------------

# Call function
msg=$(heka_cronolog)
retval=$?
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "heka.cronolog",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
