#!/bin/bash
#===============================================================================
#   DESCRIPTION:fastcache 计费日志是否堆积! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

endpoint=`hostname -s`
value=`ls /cache/logs/fcache_data | fgrep .tmp | wc -l`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.logs.jifei\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
