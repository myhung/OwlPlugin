#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_fastmedia_worker_process.sh
# Revision:     1.0
# Date:         2016/11/28
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  采集fastmedia work进程占用cpu百分比
#-------------------------------------------------------------------------------
# Revision:     1.1
# Date:         2016/12/21
# Description:  判断fastmedia多线程占用cpu百分比是否大于百分之80

#--------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fastmedia_worker_process()
{
/usr/bin/top -Hbcn 1|fgrep "fastmedia:"|fgrep worker|grep -v grep|awk '{print $9}'|sort -nr|head -1
}
#---------------------------------------------------------------------------------

# Call function
# Call function
msg=$(fastmedia_worker_process)
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "fastmedia.worker.process",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
