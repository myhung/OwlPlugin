#!/bin/bash


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function is_primer() {
    if ( grep -iqE 'KANS_TYPE.*=.*PRIMER' /usr/local/innerdns/etc/innerdns.conf 2>/dev/null ); then
        return 0
    else
        return 1
    fi
}


function parse_http_code() {

    http_code=`curl -so /dev/null -w '%{http_code}' http://innerdns.ffdns.net/`

    if ( is_primer ) && [ ! "$http_code" -ge "200" ]; then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}


msg=$(parse_http_code)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"innerdns.http.error\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
