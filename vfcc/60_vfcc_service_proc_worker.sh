#!/bin/bash

#===============================================================================
#   DESCRIPTION:vfcc worker 进程存活! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/03/28 
#===============================================================================

proc_worker(){
    worker=$(ps aux | grep -v grep | grep -c /opt1/vfcc2/worker/worker)

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.proc.worker",
        "value"         :$worker,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $vfcc -gt 0 ]];then

  proc_worker 

 else

   exit;

fi
