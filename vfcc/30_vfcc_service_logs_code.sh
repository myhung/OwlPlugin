#!/bin/bash
#===============================================================================
#   DESCRIPTION: 华数日志404监控
#       设备组:C01_I01
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/8/19 16:31
#===============================================================================

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
nowtime=`date +%Y:%H:%M`
endpoint=`hostname -s`
timestamp=`date +%s`

if [[ -f /cache/logs/wasu.cloudcdn.net.access.log ]]; then
        SUM=`fgrep "$nowtime:" /cache/logs/wasu.cloudcdn.net.access.log |grep -v HEAD |awk '$9==404{print $9}' |wc -l`

   value=$SUM
   echo -n [{\"endpoint\": \"$endpoint\", \"tags\":\"domain=wasu.com\",\"timestamp\": $timestamp, \"metric\": \"service.logs.http.code\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 30}]
else
        exit;
fi
