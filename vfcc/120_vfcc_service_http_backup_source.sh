#!/bin/bash
#===============================================================================
#   DESCRipTION:vfcc 回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/2 
# 1209 取消httping get请求
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function god_ip_alive
{
        local value=$(httping $ip -N 1 -c10 -t2 -f > /dev/null;echo $?) 

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"backup_alive=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.http.up.alive\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, 
         
}
function httpingloss
{
       local value=$(httping $ip -c10 -t2 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"backup_loss=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.http.up.loss\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},  
}
function httpingms
{
        local value=$(httping $ip -c10 -t2 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')
        if [[ -n $value ]];then

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"backup_ms=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.http.up.ms\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, 

fi
}
######################################################
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
if [ $vfcc -gt 0 ];then
[ ! -x  /usr/bin/httping ] &&  yum install -y httping-2.4
Master_source=$(cat /opt/vfcc*/nginx/conf/conf.d/upstreams.conf|grep -w server|grep -vE "#|5480|backup|down"|awk '!a[$2]++ {print $2}'|sed -e 's/:80//')
backup_source=$(cat /opt/vfcc*/nginx/conf/conf.d/upstreams.conf|grep -w server|grep -vE "#|5480|down"|grep backup|awk '!a[$2]++ {print $2}'|sed -e 's/:80//')
endpoint=$(hostname -s)
timestamp=$(date +%s)
#########################
    thread=20
    tmp_fifofile=/tmp/$$.fifo
    mkfifo $tmp_fifofile
    exec 6<>$tmp_fifofile
    rm $tmp_fifofile 

for ((i=0;i<$thread;i++));do
        echo 
done >&6

echo -n "["
 for ip in ${backup_source[@]};do 
    read -u6
    {
      god_ip_alive
      httpingloss
      httpingms
      echo >&6
    } &
  done|sed -e 's/,$/]/'
wait
exec 6>&-
exit 0
else
exit;
fi
