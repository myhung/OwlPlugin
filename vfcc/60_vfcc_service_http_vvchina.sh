#!/bin/bash
#=================================
#   DESCRIPTION:军网回源监控 
#       设备组:vfcc 超父
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/11/7 16:31
#=================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
endpoint=`hostname -s`
timestamp=`date +%s`
#########################################################################
if [[ -f /opt/vfcc/nginx/conf/vhost.d/vv.chinamil.com.cn.conf  ]]; then
   IPlist=$(cat /opt/vfcc/nginx/conf/vhost.d/vv.chinamil.com.cn.conf |egrep fail_timeout|egrep -v "#"|awk '!a[$2]++ {print $2}')
    echo -n "["
    for IP in $IPlist; do
        http_code=$(curl -s -o /dev/null -r 0-1000 -w '%{http_code}' "http://vv.chinamil.com.cn/asset/category3/2016/09/23/asset_252022.mp4" -x $IP -m 10 )
        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"军网=$IP\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http.vvcom.code\",\"value\": $http_code, \"counterType\": \"GAUGE\", \"step\": 60}, >> /tmp/check_s01_p02_vv.tmp
    done
sed -ie 's/,$/]/' /tmp/check_s01_p02_vv.tmp
cat /tmp/check_s01_p02_vv.tmp
rm -f /tmp/check_s01_p02_vv.tmp
else
	exit;
fi
