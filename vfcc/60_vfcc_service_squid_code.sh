#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
endpoint=`hostname -s`
timestamp=`date +%s`

if [ $vfcc -gt 0 ];then
        SQUID_PORT=$(fgrep 'http_port' /opt/vfcc*/squid*/etc/squid.conf|grep -v "#"|awk '{print $2}'|sed 's/127.0.0.1://')
        echo -n "["

for PORT in $SQUID_PORT; do
    
        for((i=0;i<3;i++))
        do
             LISTEN_PORT=$(nc -z -w 3 127.0.0.1 $PORT > /dev/null ;echo $?)
        if [ $LISTEN_PORT -ne 0 ];then 
             sleep 10
             continue
        fi
             LISTEN_PORT=$(nc -z -w 3 127.0.0.1 $PORT > /dev/null ;echo $?)
        if [ $LISTEN_PORT -ne 0 ];then
             sleep 20
             continue
        fi
         LISTEN_PORT=$(nc -z -w 3 127.0.0.1 $PORT > /dev/null ;echo $?)
        if [ $LISTEN_PORT -ne 0 ];then
             sleep 30
             continue
             break
        fi
        done

        echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"squid_port=$PORT\", \"timestamp\": $timestamp, \"metric\": \
                 \"vfcc.service.squid.http\",\"value\": $LISTEN_PORT, \"counterType\": \"GAUGE\", \"step\": 60},      \
                   >> /tmp/${endpoint}squid.tmp
done
        sed -ie 's/,$/]/' /tmp/${endpoint}squid.tmp
        cat /tmp/${endpoint}squid.tmp
        rm -f /tmp/${endpoint}squid.tmp
else
        exit;
fi
