#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     600_rsync_rate_error.sh
# Revision:     1.0
# Date:         2016/12/14
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  检查本机rsync是否正常
# -------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
function rsync_rate_error()
{
    rm -f /tmp/100M.file
    sleep `expr $RANDOM / 1000`
    local filename="/cache/cache/100M.file"
    local rsync_domain=`cat /etc/sysconfig/public_rsync |awk -F= '/RSYNC/{print $2}'`
    [ -e $filename ] || dd if=/dev/zero of=$filename bs=1M count=100
    rsync -v --password-file=/etc/public.pass rsync://public@$rsync_domain/rsync_cache/100M.file /tmp/ 2>&1 |awk '/received/{printf "%d%5d\n",$5/1000000,$7/1000000}' |while read file_size rsync_rate
    do
    if [ -n "$file_size" -a $file_size -gt 90 ]
    then
    echo $rsync_rate 
    else
    rm -f /tmp/100M.file
    rsync -v --password-file=/etc/public.pass rsync://public@$rsync_domain/rsync_cache/100M.file /tmp/ 2>&1 |awk '/received/{printf "%d\n",$7/1000000}'
    fi
    done
}
#---------------------------------------------------------------------------------
# Call function
msg=$(rsync_rate_error)
date=`date +%s`
host=`hostname -s`
tag=""
# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "rsync.rate.error",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 600
  }
]
EOF

