#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_powza_soft_connect.sh
# Revision:     1.0
# Date:         2017/3/6
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  检查/cache是否有软连接
#-------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function powza_soft_connect()
{
file_type=`ls -ld /cache |cut -c 1`
[ $file_type = "d" ] && return 1 || return 0
}
#---------------------------------------------------------------------------------

# Call function
msg=$(powza_soft_connect)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "powza.soft_connect",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
